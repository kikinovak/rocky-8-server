#!/bin/bash
#
# setup.sh
#
# (c) Niki Kovacs 2022 <info@microlinux.fr>
#
# This script turns a minimal Rocky Linux 8.x installation into a more
# functional base system suitable for various server configurations.

# Current directory
CWD=$(pwd)

# Default to english for admin tasks
LANG=en_US.utf8
export LANG

# Slow things down a bit
SLEEP=1

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then
  echo
  echo "  Please run with sudo or as root." >&2
  echo
  exit 1
fi

# Make sure we're running Rocky Linux 8.x.
if [ -f /etc/os-release ]
then

  source /etc/os-release

  SYSTEM="${REDHAT_SUPPORT_PRODUCT}"
  VERSION="$(awk -F'[".]' '/^VERSION_ID=/ {print $2}' /etc/os-release)"

fi

if [ "${SYSTEM}" != "Rocky Linux" ] && [ "${VERSION}" != "8" ] 
then

  echo
  echo "  Unsupported operating system." >&2
  echo

  exit 1

fi

sleep ${SLEEP}
echo
echo "  ######################################"
echo "  # Rocky Linux ${VERSION} server configuration #"
echo "  ######################################"
echo
sleep ${SLEEP}

# Defined users
USERS="$(awk -F: '$3 > 999 && $3 < 65534 {print $1}' /etc/passwd | sort)"

# Admin user
ADMIN=$(getent passwd 1000 | cut -d: -f 1)

# Basic tools
TOOLS=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/el8/pkglists/tools.txt)

# Enhanced base system
BASE=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/el8/pkglists/base.txt)

# Mirrors
DOCKER="https://download.docker.com/linux/centos"
CISOFY="https://packages.cisofy.com"
ICINGA="https://packages.icinga.com"
SQUIDA="https://download.copr.fedorainfracloud.org/results/tachtler/squidanalyzer"
REMI="https://rpms.remirepo.net/enterprise"

# Log
LOG="/var/log/$(basename "${0}" .sh).log"

logentry() {

  echo >> ${LOG}
  echo "+---------------------------+" >> ${LOG}
  echo "$(date)" >> ${LOG}
  echo >> ${LOG}

}

usage() {

  # Display help message
  echo "  Usage: ${0} OPTION"
  echo
  echo "  Rocky Linux ${VERSION} server configuration."
  echo
  echo "  Options:"
  echo
  echo "    --shell    Configure shell: Bash, Vim and system locale."
  echo "    --repos    Setup official and third-party repositories."
  echo "    --fresh    Sync repositories and fetch updates."
  echo "    --tools    Install full set of command-line tools."
  echo "    --ipv4     Disable IPv6 and reconfigure basic services."
  echo "    --setup    Perform all of the above in one go."
  echo "    --ipv6     Reactivate IPv6 if it has been disabled."
  echo "    --reset    Revert back to enhanced base system."
  echo
  echo "  Logs are written to ${LOG}."
  echo
}

configure_shell() {

  logentry

  echo "  === Shell configuration ==="
  echo
  sleep ${SLEEP}

  echo "  Configuring Bash shell for user: root"
  cp -vf ${CWD}/el8/bash/bashrc-root /root/.bashrc >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Bash shell for future users."
  cp -vf ${CWD}/el8/bash/bashrc-users /etc/skel/.bashrc >> ${LOG}
  sleep ${SLEEP}

  if [ ! -z "${USERS}" ]
  then

    for USER in ${USERS}
    do

      if [ -d /home/${USER} ]
      then

        echo "  Configuring Bash shell for user: ${USER}"
        cp -vf ${CWD}/el8/bash/bashrc-users /home/${USER}/.bashrc >> ${LOG}
        chown -v ${USER}:${USER} /home/${USER}/.bashrc >> ${LOG}
        sleep ${SLEEP}

      fi

    done

  fi

  echo "  Configuring Vim editor for user: root"
  cp -vf ${CWD}/el8/vim/vimrc /root/.vimrc >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Vim editor for future users."
  cp -vf ${CWD}/el8/vim/vimrc /etc/skel/.vimrc >> ${LOG}
  sleep ${SLEEP}

  if [ ! -z "${USERS}" ]
  then

    for USER in ${USERS}
    do

      if [ -d /home/${USER} ]
      then

        echo "  Configuring Vim editor for user: ${USER}"
        cp -vf ${CWD}/el8/vim/vimrc /home/${USER}/.vimrc >> ${LOG}
        chown -v ${USER}:${USER} /home/${USER}/.vimrc >> ${LOG}
        sleep ${SLEEP}

      fi

    done

  fi

  # Support for en_US.utf8
  if ! rpm -q glibc-langpack-en > /dev/null 2>&1
  then
    echo "  Installing basic support for system locale."
    dnf -y install glibc-langpack-en >> ${LOG} 2>&1
  fi
  echo "  Configuring system locale."
  localectl set-locale LANG=en_US.utf8
  sleep ${SLEEP}

  # Don't inherit system locale
  echo "  Configuring SSH server."
  sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
  systemctl reload sshd
  sleep ${SLEEP}

  # Password for sudo
  echo "  Configuring persistent password for sudo."
  cp -vf ${CWD}/el8/sudoers.d/persistent_password \
    /etc/sudoers.d/persistent_password >> ${LOG}
  sleep ${SLEEP}

  # Admin user can access system logs
  if [ ! -z "${ADMIN}" ]
  then
    if getent group systemd-journal | grep ${ADMIN} > /dev/null 2>&1
    then
      echo "  Admin user ${ADMIN} is already a member of the systemd-journal group."
    else
      echo "  Adding admin user ${ADMIN} to systemd-journal group."
      usermod -aG systemd-journal ${ADMIN}
    fi
    sleep ${SLEEP}
  fi

  echo

}

configure_repos() {
  
  logentry

  echo " === Package repository configuration ==="
  echo 
  sleep ${SLEEP}

  echo "  Removing existing repositories."
  rm -f /etc/yum.repos.d/*.repo
  rm -f /etc/yum.repos.d/*.repo.rpmsave
  sleep ${SLEEP}

  # Enable BaseOS, Appstream, Extras and PowerTools repositories with a
  # priority of 1.
  for REPOSITORY in BaseOS AppStream Extras PowerTools
  do
    echo "  Enabling repository: ${REPOSITORY}"
    cp -vf ${CWD}/el8/dnf/Rocky-${REPOSITORY}.repo /etc/yum.repos.d/ >> ${LOG}
    sleep ${SLEEP}
  done

  # Enable EPEL repository with a priority of 10.
  echo "  Enabling repository: EPEL" 
  if ! rpm -q epel-release > /dev/null 2>&1
  then
    dnf -y install epel-release >> ${LOG} 2>&1
  fi
  cp -vf ${CWD}/el8/dnf/epel.repo /etc/yum.repos.d/epel.repo >> ${LOG}
  sleep ${SLEEP}

  # Enable EPEL Modular repository with a priority of 10.
  echo "  Enabling repository: EPEL Modular" 
  cp -vf ${CWD}/el8/dnf/epel-modular.repo \
    /etc/yum.repos.d/epel-modular.repo >> ${LOG}
  sleep ${SLEEP}

  # Disable EPEL Testing repository
  echo "  Removing repository: EPEL Testing"
  rm -f /etc/yum.repos.d/epel-testing.repo
  sleep ${SLEEP}

  # Disable EPEL Modular Testing repository
  echo "  Removing repository: EPEL Modular Testing"
  rm -f /etc/yum.repos.d/epel-testing-modular.repo
  sleep ${SLEEP}

  # Disable EPEL Playground repository
  echo "  Removing repository: EPEL Playground"
  rm -f /etc/yum.repos.d/epel-playground.repo
  sleep ${SLEEP}

  # Install Remi repository configuration
  echo "  Installing repository: Remi" 
  if ! rpm -q remi-release > /dev/null 2>&1
  then
    dnf -y install ${REMI}/remi-release-8.rpm >> ${LOG} 2>&1
  fi
  sleep ${SLEEP}

  # Enable Remi Safe repository with a priority of 10.
  echo "  Enabling repository: Remi Safe" 
  cp -vf ${CWD}/el8/dnf/remi-safe.repo \
    /etc/yum.repos.d/remi-safe.repo >> ${LOG}
  sleep ${SLEEP}

  # Enable Remi Modular repository with a priority of 10.
  echo "  Enabling repository: Remi Modular" 
  cp -vf ${CWD}/el8/dnf/remi-modular.repo \
    /etc/yum.repos.d/remi-modular.repo >> ${LOG}
  sleep ${SLEEP}

  # Disable Remi repository
  echo "  Removing repository: Remi"
  rm -f /etc/yum.repos.d/remi.repo
  sleep ${SLEEP}

  # Enable ELRepo repository with a priority of 10.
  echo "  Enabling repository: ELRepo"
  if ! rpm -q elrepo-release > /dev/null 2>&1
  then
    dnf -y install elrepo-release >> ${LOG} 2>&1
  fi
  cp -vf ${CWD}/el8/dnf/elrepo.repo /etc/yum.repos.d/elrepo.repo >> ${LOG}
  sleep ${SLEEP}

  # Enable Lynis repository with a priority of 5.
  echo "  Enabling repository: Lynis"
  rpm --import ${CISOFY}/keys/cisofy-software-rpms-public.key >> ${LOG} 2>&1
  cp -vf ${CWD}/el8/dnf/lynis.repo /etc/yum.repos.d/lynis.repo >> ${LOG}
  sleep ${SLEEP}

  # Configure Icinga repository with a priority of 10.
  echo "  Enabling repository: Icinga"
  if ! rpm -q icinga-rpm-release > /dev/null 2>&1
  then
    dnf -y install ${ICINGA}/epel/icinga-rpm-release-8-latest.noarch.rpm >> ${LOG} 2>&1
  fi
  cp -vf ${CWD}/el8/dnf/ICINGA-release.repo /etc/yum.repos.d/ >> ${LOG} 2>&1
  rm -f /etc/yum.repos.d/ICINGA-snapshot.repo
  sleep ${SLEEP}

  # Configure Docker repository with a priority of 10.
  echo "  Enabling repository: Docker"
  rpm --import ${DOCKER}/gpg >> ${LOG} 2>&1
  cp -vf ${CWD}/el8/dnf/docker-ce.repo /etc/yum.repos.d/ >> ${LOG} 2>&1
  sleep ${SLEEP}

  # Enable SquidAnalyzer repository with a priority of 10.
  echo "  Enabling repository: SquidAnalyzer"
  rpm --import ${SQUIDA}/pubkey.gpg
  cp -vf ${CWD}/el8/dnf/squidanalyzer.repo /etc/yum.repos.d/ >> ${LOG}
  sleep ${SLEEP}

  echo

}

update_system() {

  logentry

  echo "  === Update system ==="
  echo
  sleep ${SLEEP}

  if ! rpm -q drpm > /dev/null 2>&1
  then

    echo "  Enabling Delta RPM."
    dnf -y install drpm >> ${LOG} 2>&1

  fi

  echo "  Cleaning package cache."
  dnf clean all >> ${LOG} 2>&1
  sleep ${SLEEP}

  echo "  Downloading metadata for enabled repositories."
  dnf makecache >> ${LOG} 2>&1

  echo "  Performing system update."
  sleep ${SLEEP}
  echo "  This might take a moment..."
  dnf -y update >> ${LOG} 2>&1

  echo

}

install_tools() {

  logentry

  echo "  === Install basic system tools ==="
  echo
  sleep ${SLEEP}

  echo "  Fetching missing packages from Core package group." 
  dnf -y group mark remove "Core" >> ${LOG} 2>&1
  dnf -y group install "Core" >> ${LOG} 2>&1
  echo "  Core package group installed on the system."
  sleep ${SLEEP}

  echo "  Installing Base package group."
  sleep ${SLEEP}
  echo "  This might take a moment..."
  dnf -y group mark remove "Base" >> ${LOG} 2>&1
  dnf -y group install "Base" >> ${LOG} 2>&1
  echo "  Base package group installed on the system."
  sleep ${SLEEP}

  echo "  Installing some additional packages."
  sleep ${SLEEP}
  for PACKAGE in ${TOOLS}
  do
    if ! rpm -q ${PACKAGE} > /dev/null 2>&1
    then
      echo "  Installing package: ${PACKAGE}"
      dnf -y install ${PACKAGE} >> ${LOG} 2>&1
    fi
  done

  echo "  Basic system tools installed on the system."
  echo
  sleep ${SLEEP}

}

disable_ipv6() {

  echo "  === Use IPv4 only ==="
  echo
  sleep ${SLEEP}

  echo "  Disabling IPv6."
  sleep ${SLEEP}

  rm -f /etc/sysctl.d/enable-ipv6.conf
  cp -vf ${CWD}/el8/sysctl.d/disable-ipv6.conf /etc/sysctl.d/ >> ${LOG} 2>&1
  sysctl -p --load /etc/sysctl.d/disable-ipv6.conf >> ${LOG} 2>&1

  # Reconfigure SSH 
  if [ -f /etc/ssh/sshd_config ]
  then
    echo "  Configuring SSH server for IPv4 only."
    sleep ${SLEEP}
    sed -i -e 's/#AddressFamily any/AddressFamily inet/g' /etc/ssh/sshd_config
    sed -i -e 's/#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/g' /etc/ssh/sshd_config
    systemctl reload sshd
  fi

  # Reconfigure Postfix
  if [ -f /etc/postfix/main.cf ]
  then
    echo "  Configuring Postfix server for IPv4 only."
    sleep ${SLEEP}
    sed -i -e 's/# Enable IPv4, and IPv6 if supported/# Enable IPv4/g' /etc/postfix/main.cf
    sed -i -e 's/inet_protocols = all/inet_protocols = ipv4/g' /etc/postfix/main.cf
    systemctl restart postfix
  fi

  # Rebuild initrd
  echo "  Rebuilding initial ramdisk."
  dracut -f -v >> ${LOG} 2>&1

  echo

}

enable_ipv6() {

  echo "  === Reactivate IPv6 ==="
  echo
  sleep ${SLEEP}

  echo "  Enabling IPv6."
  sleep ${SLEEP}

  rm -f /etc/sysctl.d/disable-ipv6.conf
  cp -vf ${CWD}/el8/sysctl.d/enable-ipv6.conf /etc/sysctl.d/ >> ${LOG} 2>&1
  sysctl -p --load /etc/sysctl.d/enable-ipv6.conf >> ${LOG} 2>&1

  # Reconfigure SSH 
  if [ -f /etc/ssh/sshd_config ]
  then
    echo "  Consider reactivating IPv6 in your SSH server configuration."
    sleep ${SLEEP}
  fi

  # Reconfigure Postfix
  if [ -f /etc/postfix/main.cf ]
  then
    echo "  Consider reactivating IPv6 in your Postfix server configuration."
    sleep ${SLEEP}
  fi

  # Rebuild initrd
  echo "  Rebuilding initial ramdisk."
  dracut -f -v >> ${LOG} 2>&1

  echo

}

reset_system() {
  echo "  === Restore enhanced base system ==="
  echo
  sleep ${SLEEP}
  # Display all packages that are not part of the enhanced base system.
  echo "  Creating database."
  local TMP="/tmp"
  local PKGLIST="${TMP}/pkglist"
  local PKGINFO="${TMP}/pkg_base"
  rpm -qa --queryformat '%{NAME}\n' | sort > ${PKGLIST}
  PACKAGES=$(egrep -v '(^\#)|(^\s+$)' $PKGLIST)
  rm -rf ${PKGLIST} ${PKGINFO}
  mkdir ${PKGINFO}
  unset REMOVE
  for PACKAGE in ${BASE}
  do
    touch ${PKGINFO}/${PACKAGE}
  done
  for PACKAGE in ${PACKAGES}
  do
    if [ -r ${PKGINFO}/${PACKAGE} ]
    then
      continue
    else
      REMOVE="${REMOVE}\n  * ${PACKAGE}"
    fi
  done
  if [ ! -z "${REMOVE}" ]
  then
    echo
    echo "  The following packages are not part of the enhanced base system:"
    echo -e "${REMOVE}"
  fi
  rm -rf ${PKGLIST} ${PKGINFO}
  echo
}

# Check parameters.
if [[ "${#}" -ne 1 ]]
then
  usage
  exit 1
fi
OPTION="${1}"
case "${OPTION}" in
  --shell)
    configure_shell
    ;;
  --repos)
    configure_repos
    ;;
  --fresh)
    update_system
    ;;
  --tools)
    install_tools
    ;;
  --ipv4) 
    disable_ipv6
    ;;
  --ipv6) 
    enable_ipv6
    ;;
  --setup) 
    configure_shell
    configure_repos
    update_system
    install_tools
    disable_ipv6
    ;;
  --reset) 
    reset_system
    ;;
  --help) 
    usage
    exit 0
    ;;
  ?*) 
    usage
    exit 1
esac

exit 0
