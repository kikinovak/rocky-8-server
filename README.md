# Post-installation setup script for Rocky Linux 8.x servers 

(c) Niki Kovacs, 2022

This repository provides an "automagic" post-installation setup script for
servers running Rocky Linux 8.x. No fancy configuration management here. Just a
bone-headed shell script following the [KISS
principle](https://en.wikipedia.org/wiki/KISS_principle).

## In a nutshell

Perform the following steps.

  - Install a minimal Rocky Linux 8.x system.

  - Install Git: `dnf install -y git`

  - Grab the script: `git clone https://gitlab.com/kikinovak/rocky-8-server`

  - Change into the new directory: `cd rocky-8-server`

  - Run the script: `./setup.sh --setup`

  - Grab a cup of coffee while the script does all the work.

  - Reboot.

## Customizing a Rocky Linux server

Turning a minimal Rocky Linux installation into a functional server always
boils down to a series of more or less time-consuming operations. Your mileage
may vary of course, but here's what I usually do on a fresh Rocky Linux
installation:

  * Customize the Bash shell : prompt, aliases, locales, etc.

  * Customize the Vim editor.

  * Setup official and third-party repositories.

  * Install a complete set of command line tools.

  * Disable IPv6 and reconfigure some services accordingly.
  
The `setup.sh` script performs all of these operations.

Configure Bash and Vim and use english as main system locale:

```
# ./setup.sh --shell
```

Setup official and third-party repositories:

```
# ./setup.sh --repos
```

Enable DeltaRPM and fetch system updates:

```
# ./setup.sh --fresh
```

Install the `Core` and `Base` package groups along with some extra tools:

```
# ./setup.sh --tools
```

Disable IPv6 and reconfigure basic services accordingly:

```
# ./setup.sh --ipv4
```

Perform all of the above in one go:

```
# ./setup.sh --setup
```

Display all packages that don't belong to the enhanced base system:

```
# ./setup.sh --reset
```

Reactivate IPv6 if needed:

```
# ./setup.sh --ipv6
```

Display help message:

```
# ./setup.sh --help
```

If you want to know what exactly goes on under the hood, open a second terminal
and view the logs:

```
$ tail -f /var/log/setup.log
```

&nbsp;

---

*Click on the cup and buy me a coffee.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
