#!/bin/bash
#
# mkcert.sh

# Create certs group
if [ ! $(getent group certs) ] 
then
  echo "Adding group certs with GID 240."
  groupadd -g 240 certs
fi

# Stop Apache
if ps ax | grep -v grep | grep httpd > /dev/null
then
  echo "Stopping Apache server."
  systemctl stop httpd 1 > /dev/null 2>&1
fi

# Generate SSL/TLS certificate
certbot certonly \
  --non-interactive \
  --email info@microlinux.fr \
  --preferred-challenges http \
  --standalone \
  --agree-tos \
  --force-renewal \
  -d sd-155842.dedibox.fr \
  -d slackbox.fr \
  -d mail.slackbox.fr \
  -d www.slackbox.fr \
  -d unixbox.fr \
  -d mail.unixbox.fr \
  -d www.unixbox.fr

# Permissions
echo "Setting file permissions."
chgrp -R certs /etc/letsencrypt
chmod -R g=rx /etc/letsencrypt

# Start Apache
echo "Starting Apache server."
systemctl start httpd

exit 0
