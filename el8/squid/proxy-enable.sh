#!/bin/bash
# 
# proxy-enable.sh
firewall-cmd --permanent --add-service=squid
firewall-cmd --permanent --add-forward-port=port=80:proto=tcp:toport=3128:toaddr=192.168.2.1
firewall-cmd --permanent --add-port=3129/tcp
firewall-cmd --permanent --add-forward-port=port=443:proto=tcp:toport=3129:toaddr=192.168.2.1
firewall-cmd --reload
firewall-cmd --list-all
